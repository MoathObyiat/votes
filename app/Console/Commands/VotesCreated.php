<?php

namespace App\Console\Commands;

use Event;
use App\Events\VotesCreated as votesSender;

use Illuminate\Console\Command;

class VotesCreated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'votes:created';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily email with votes created last 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Event::fire(new votesSender());
    }
}
