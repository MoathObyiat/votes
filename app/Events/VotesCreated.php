<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class VotesCreated extends Event
{
    use SerializesModels;
    /**
     * @var \App\Models\Question
     */
    public $votes;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\Question $question
     */
    public function __construct()
    {
        
    }
}
