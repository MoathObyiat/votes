<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ResetPass extends Event
{
    use SerializesModels;

    public $client;

    public function __construct($client)
    {
        $this->client = $client;
    }
}
