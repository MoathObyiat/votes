<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Clients;
use Illuminate\Queue\SerializesModels;

class SignUp extends Event
{
    use SerializesModels;

    public $change;
    public $client;

    public function __construct($change, $client_id)
    {
        $this->change = $change;
        $this->client = Clients::find($client_id);
    }
}
