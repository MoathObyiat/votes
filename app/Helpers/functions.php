<?php

use App\Models\Vote;
use App\Models\Voters;
use App\Models\Question;

use Illuminate\Support\Facades\Auth;

function isAllowed($user_id)
{
	$access = false;
	if (Auth::user()->id == $user_id)
		$access = true;

	return $access;
}

function validateFaker($question_id, $answer_id)
{
	$voter_id = Voters::all()->random()->id;
	$vote = Vote::where('question_id', $question_id)->where('voter_id', $voter_id)->exists();
	
	if(!$vote)
		Vote::create([
            'voter_id'     => $voter_id,
	        'question_id'  => $question_id,
	        'answer_id'    => $answer_id,
        ]);

}

function isVoted($question_id)
{
	return Vote::where('question_id', $question_id)->where('voter_id', Auth::user()->id)->exists();

	// if ($data)
	// 	return true;
	// else
	// 	return false;



}