<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $table = 'votes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'voter_id',
        'question_id',
        'answer_id'
    ];

    public function question()
    {
        return $this->belongsTo('App\Models\Question','question_id');
    }

    public function answer()
    {
        return $this->belongsTo('App\Models\Answer','answer_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\Voters','voter_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

}
