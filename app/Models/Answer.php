<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Answer extends BaseModel
{
    protected $table = 'answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'question_id',
    ];

    public function question()
    {
        return $this->belongsTo('App\Models\Question', 'question_id');
    }



}
