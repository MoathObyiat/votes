<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends BaseModel
{
	protected $table = 'questions';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'title',
		'user_id',
		'expired_date'
	];

	public function user()
    {
        return $this->belongsTo('App\Models\Question', 'user_id');
    }

    /**
     * Get the answers for the question.
     */
    public function answers()
    {
        return $this->hasMany('App\Models\Answer');
    }

    /**
     * Get the vote for the question.
     */
    public function vote()
    {
        return $this->hasMany('App\Models\Vote');
    }

}
