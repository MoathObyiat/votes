<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class  QuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
        // dd($this->answers, is_null($this->answers));
        $roues = [
          'title'         => 'required',
          'expired_date'  => 'required',
        ];

        if(!empty($this->answers))
            $roues['answers'] = 'present|array|min:2';

        return $roues;
      
     }
}
