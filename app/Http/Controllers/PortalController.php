<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\Vote;
use App\Models\Voters;
use App\Models\Question;
use Illuminate\Http\Request;

class PortalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:portal');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total     = Voters::count();

        $questions = Question::with(['answers', 'vote' => function($query){
            $query->select('*', DB::raw('(CASE WHEN voter_id = ' . Auth::user()->id . ' THEN 1 ELSE 0 END) AS is_votes'));
          }
          ])->orderBy('id', 'desc')->paginate(15);

        // dd($questions);

        return view('portal', compact('questions', 'total'));
    }

    public function saveVotes(Request $request)
    {
        Vote::create([
            'voter_id'     => Auth::user()->id,
            'question_id'  => $request->question_id,
            'answer_id'    => $request->answer_id,
        ]);

        return redirect(route('portal.index'));
    }
}
