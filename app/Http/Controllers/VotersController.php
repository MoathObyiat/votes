<?php

namespace App\Http\Controllers;

use App\Models\Voters;
use Illuminate\Http\Request;

class VotersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voters  $voters
     * @return \Illuminate\Http\Response
     */
    public function show(Voters $voters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voters  $voters
     * @return \Illuminate\Http\Response
     */
    public function edit(Voters $voters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voters  $voters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voters $voters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voters  $voters
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voters $voters)
    {
        //
    }
}
