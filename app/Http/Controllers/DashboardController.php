<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\Models\Vote;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    
    public function index()
    {
        $vote_data = [];

        //Get total votes and group date.
    	$data = Vote::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
             ->groupBy(DB::raw('DATE(created_at)'))
             ->get()->toArray();
        
        foreach ($data as $key => $value) {
            $dataset = [
                "label" => $value['date'],
                'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                'borderColor' => "rgba(38, 185, 154, 0.7)",
                "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => [
                    ["x"=> $value['date'], "y" => $value['total']]
                ],
            ];

            array_push($vote_data, $dataset);
    	}

    	$chartjs = app()->chartjs
	        ->name('lineChartTest')
	        ->type('line')
	        ->size(['width' => 400, 'height' => 200])
	        ->datasets($vote_data)
	        ->options([
	        	'scales' => [
		        	"xAxes" => [[
	                    "type" => "time",
	                    "time" => [
	                    	"unit" => 'day',
	                        "displayFormats" => [
			                    "day" => 'YYYY-MM-DD',
			                 ]
	                    ],
	                    "scaleLabel" => [
	                        "display" =>  "true",
	                        "labelString" => 'Date'
	                    ]

	                ]],
	                "yAxes" => [[
	                    "scaleLabel" => [
	                        "display" =>  "true",
	                        "labelString" => 'value'
	                    ]
	                ]]
	            ]
			]);
        
        return view('dashboard.index', compact('chartjs'));
    }

   
}
    