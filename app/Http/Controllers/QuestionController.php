<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Question;
use App\Models\Answer;

use App\Http\Requests\QuestionRequest;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $questions =  Question::orderBy('id', 'desc')->get();
        return view('question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
    	$answers = [];
        return view('question.create', compact('answers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param QuestionRequest $request
     * @return Response
     */
    public function store(QuestionRequest $request)
    {
    	//Save question.
        $question = Question::create(array_merge($request->all(), ['user_id' => Auth::user()->id]));

        //Save Answer.
        foreach ($request['answers'] as $key => $value) {
        	if ($value) {
	        	$answers = Answer::create([
	        		'title' 	  => $value,
	        		'question_id' => $question->id
	        	]);
        	}
        }

        flash()->success('Question created!');
        return redirect(route('question.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $question       = Question::findOrFail($id);
        $answers        = Answer::where('question_id', $id)->get(['title']);

        return view('question.edit', compact('question', 'answers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  QuestionRequest $request
     * @return Response
     */
    public function update(QuestionRequest $request, $id)
    {
        Question::findOrFail($id)->update($request->all());

        foreach ($request['answers'] as $key => $value) {
        	if ($value) {
	        	$answers = Answer::updateOrCreate([
	        		'title' 	  => $value,
	        		'question_id' => $id
	        	]);
        	}
        }

        flash()->success('Question updated!');
        return redirect(route('question.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
    	//Remove Answer
    	Answer::where('question_id', $id)->delete();

        return Question::destroy($id);
    }

    public function show($id)
    {
    	$question  = Question::with('answers')->where('id', $id)->first();

    	return view('question.show', compact('question'));
    }
}
