<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\Models\Question;

use App\Events\VotesCreated;
use Illuminate\Contracts\Mail\Mailer;

class VotesCreatedListener
{
    /**
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    private $mailer;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     */
    
    public function handle()
    {
        $data = Question::where('created_at', '>=', Carbon::now()->subDay()->toDateTimeString())->get();

        $this->mailer->send('emails.votes_created', compact('data'),
            function ($mail) use ($data) {
                $mail->to(env('ADMIN_EMAIL'))->subject("Votes created last 24 hours");
        });
       
    }
}
