<?php

namespace App\Listeners;

use App\Events\ResetPass;
use Illuminate\Contracts\Mail\Mailer;

class ResetPassSender
{
    /**
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param \Illuminate\Contracts\Mail\Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        //
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  activeClientSent  $event
     * @return void
     */
    public function handle(ResetPass $event)
    {
        
        $client = $event->client;
        $password = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);
        
        $client->update(['password' => bcrypt($password)]);

        $this->mailer->send('emails.sendResetPass', compact(['client', 'password']), function ($mail) use ($client) {
            $mail->to($client->email)->subject('Reset Password Request on Prema Group Website');
        });

    }
}
