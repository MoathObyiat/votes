<?php

return [



    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute contains invalid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    // 'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',
    
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],



    // 'custom' =>
    //  [
    //     'image' =>
    //      [
    //         'required'            => 'The Image field is required mohammad ali',
    //         'rule-dimensions'     => 'The image is has dimensions error value',
    //      ],          
    //  ],




     'custom' => 
     [
          'password' => ''

     ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

  'attributes' => [

    'cemetery_id'                   => 'cemetery name',
    'title_en'                      => 'title (English)',
    'title_ar'                      => 'title (Arabic)',
    'description_en'                => 'description (English)',
    'description_ar'                => 'description (Arabic)',
    'our_mission_en'                => 'our mission (English)',
    'our_mission_ar'                => 'our mission (Arabic)',
    'our_vision_ar'                 => 'our vision (Arabic)',
    'our_vision_en'                 => 'our vision (English)',
    'person_name_en'                => 'person name (English)',
    'person_name_ar'                => 'person name (Arabic)',
    'vitas_group_en'                => 'vitas group (English)',
    'vitas_group_ar'                => 'vitas group (Arabic)',
    'message_en'                    => 'general manager message (English)',
    'message_ar'                    => 'general manager message (Arabic)',
    'management_en'                 => 'our management (English)',
    'management_ar'                 => 'our management (Arabic)' ,
    'contact_us_email_others'       => 'contact email (Others)',
    'contact_us_email_compliants'   => 'contact email (complaint)',
    'contact_us_email_suggestions'  => 'contact email (Suggestions)',
    'text_ar'                       => 'text (Arabic)',
    'text_en'                       => 'text (English)',
    'start_date'                    => 'Start Date',
    'end_date'                      => 'End Date',
    'name_en'                       => 'Name (English)',
    'name_ar'                       => 'Name (Arabic)',
    'branch_en'                     => 'branch (English)',
    'branch_ar'                     => 'branch (Arabic)',
    'address_en'                    => 'address (English)',
    'address_ar'                    => 'address (Arabic)',
    'position_en'                   => 'Position (English)',
    'position_ar'                   => 'Position (Arabic)',
    'location_ar'                   => 'Location (Arabic)',
    'location_en'                   => 'Location (English)',
    'position_id'                   => 'position',
    'location_id'                   => 'location',
    'question_en'                   => 'Question (English)',
    'question_ar'                   => 'Question (Arabic)',
    'question_ar'                   => 'Question (Arabic)',
    'option_en_1'                   => 'Option 1 (English)',
    'option_ar_1'                   => 'Option 1 (Arabic)',
    'option_en_2'                   => 'Option 2 (English)',
    'option_ar_2'                   => 'Option 2 (Arabic)',
    'location_ar'                   => 'location (Arabic)',
    'location_en'                   => 'location (English)',
    'surveis_email'                 => 'surveys email',
    'facebook'                      => 'facebook field',
    'youtube'                       => 'youtube field',
    'linkedIn'                      => 'linkedin field',
    'linkedin'                      => 'linkedin field',
    'twitter'                       => 'twitter field',
    'instagram'                     => 'instagram field',
    'message_image'                 => 'image',
    'permission_id'                 => 'permitted modules',
    'role_title'                    => 'role',
    'media_path'                    => 'media file',
    'role_id'                       => 'role',
    'personal_image'                => 'personal image',
    'android_link'                  => 'google play link',
    'apple_link'                    => 'app store link',
    ],

];
