<?php

return [
    'arabic'                => 'Arabic',
    'english'               => 'English',
    'dashboard'             => 'Dashboard',
    'settings'              => 'Settings',
    'home_page'             => 'Home Page',
    'my_profile'            => 'My profile',
    'logout'                => 'Logout',
    'question'              => 'Questions',
];
