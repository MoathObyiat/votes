@extends('email')

@section('content')
	
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="background-table">
<tbody>
<tr>
<td align="center" bgcolor="#f2f2f2">
<table class="w640" style="margin:0 10px;" width="640" cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td class="w640" width="640" height="20"></td>
</tr>
<tr>
<td class="w640" width="640" height="30" bgcolor="#ffffff"></td>
</tr>
<tr id="simple-content-row">
<td class="w640" width="640" bgcolor="#ffffff">
    <table class="w640" width="640" cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <tr>
            <td class="w30" width="30"></td>
            <td class="w580" width="580">
                <table class="w580" width="580" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                    <tr>
                        <td class="w580" width="580">
                            <div align="left" class="article-content">
                                <multiline label="Description">
                                    Dear Sir,
                                    This email contain votes created last 24 hours:
                                </multiline>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="w580" width="580" height="10"></td>
                    </tr>
                    </tbody>
                </table>
        </tr>

        <tr>
            <td class="w30" width="30"></td>
            <td class="w580" width="580">
                <table class="w580" width="580" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        <tr>
                            <td class="w275" width="275">
                                @foreach($data as $value)
                                <div align="left" class="article-content">
                                    <singleline label="Description">

                                        <strong>ID:</strong> 
                                        {{ $value['id'] }}
                                    
                                    </singleline>
                                    <br>
                                    <singleline label="Description">

                                        <strong>Title:</strong> 
                                        {{ $value['title'] }}
                                    
                                    </singleline>
                                    <br>
                                    <singleline label="Description">

                                        <strong>Expire Date:</strong> 
                                        {{ $value['expired_date'] }}
                                    
                                    </singleline>
                                </div>
                                @endforeach
                               
                            <td>     
                        </tr>
                    <tr>
                        <td class="w580" width="580" height="10"></td>
                    </tr>
                    </tbody>
                </table>
        </tr>

</tbody>
</table>                                        
</tr>
<tr>
<td class="w640" width="640" height="30"></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>
    
@endsection