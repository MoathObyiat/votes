<li class="dropdown">
    <a data-toggle="dropdown" class="tm-notification" href="">
        <i class="tmn-counts">{!! count($notifications) !!}</i>
    </a>

    <div class="dropdown-menu dropdown-menu-lg pull-right">
        <div class="listview" id="notifications">
            <div class="lv-header">
                Notification

                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-clear="notification">
                            <i class="md md-done-all"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="lv-body c-overflow">
                @foreach ($notifications as $notification)
                    <a class="lv-item" href="{{ url($notification->name) }} ">
                        <div class="media">
                            <div class="pull-left">
                                <img class="lv-img-sm" src="/img/profile-pics/1.jpg" alt="">
                            </div>
                            <div class="media-body">
                                <div class="lv-title">{!! $notification->name !!}</div>
                                <small class="lv-small">
                                    {!! $notification->description !!}
                                </small>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

    </div>
</li>