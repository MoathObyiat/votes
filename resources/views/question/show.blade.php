@extends('app')

@section('content')
    <div class="block-header">
        <h2>View  {{ $question->title }}</h2>

    </div>

    <div class="card">
        <div class="card-body card-padding">
            <div class="pmbb-body p-l-30 p-t-30">
                <div class="pmbb-view">
                    
                    <div class="row">
                        <div class="col-md-6">
                             <dl class="dl-horizontal">
                                <dt> Qustion Title</dt>
                                <dd class="setting-card">{{ $question->title }}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Expired Date</dt>
                        <dd class="setting-card">{{ $question->expired_date }}</dd>
                            </dl>   
                        </div>
                    </div>

                    <div class="row">
                        <dt> Answers</dt>
                        <dl class="dl-horizontal">
                        @foreach($question->answers as $answer)
                            <dd class="setting-card">{{ $answer->title }}</dd><br>
                        @endforeach
                        </dl>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <a class="btn btn-info btn-sm" href="{{ route('question.index') }}">@lang('common.back')</a>
                        </div>
                    </div>

                    

                </div>
            </div>
        </div>
    </div>

@endsection
