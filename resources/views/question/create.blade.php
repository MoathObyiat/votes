@extends('app')

@section('content')
    <div class="card">

        {!! Form::open(['method' => 'POST', 'url' => route('question.index'), 'class' => 'form-horizontal']) !!}
            <div class="card-header">
                <h2>Create Question</h2>
            </div>

            @include('question.form')
        {!! Form::close() !!}
    </div>
@endsection
