@extends('app')

@section('js')
    
    var datatable = $("#data-table-command").DataTable();

    $("#data-table-command").on("click", ".command-delete", function(e){
        e.preventDefault();
        var that = $(this);
        var rowId = $(this).data('row-id');
         var url = '{{ route('question.index') }}/' + rowId + '/delete';
        swal({
            title: "@lang('common.are_you_sure')",
            text:  "When delete qustion all answers deleted!",
            type:  "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            cancelButtonText: "@lang('common.cancle_delete')",

        }, function(){
            $.post(url, {
                _token: '{{ csrf_token() }}'
            }, function() {
                var Row = that.closest('tr');
                datatable.row(Row).remove().draw();
                swal("@lang('common.deleted')", "The qustion has been deleted.", "success");
            });
        });
    });

@endsection

@section('content')
    <div class="block-header">
        <h2><li class="md-insert-drive-file"></li>Questions</h2>

        <ul class="actions">
            <li>
                <a href="{{ route('question.index') }}/create" class="btn btn-icon command-add" data-toggle="tooltip" data-placement="bottom" data-original-title="Add"><span class="md mk md-add"></span></a>
            </li>
        </ul>
    </div>

    <div class="card">
        <div class="card-header">
        </div>

        <table id="data-table-command" class="uk-table uk-table-hover uk-table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Expire Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($questions as $question)
                <tr>
                    <td>{{  $question->id }}</td>
                    <td>{{  $question->title }}</td>
                    <td>{{  $question->expired_date }}</td>
                    <td class="td-spical">

                            <a href="{{ route('question.show', $question->id) }}" class="btn btn-icon command-show" data-toggle="tooltip" data-placement="top" data-original-title="Show {{ $question->title }}"><span class="zmdi mk zmdi-eye"></span></a>

                           <a class="btn btn-icon command-edit" href="question/{{ $question->id }}/edit" data-row-id="{{ $question->id }}" data-toggle="tooltip" data-placement="top" data-original-title="@lang('common.edit') {{ $question->title }}">
                               <i class="md mk md-edit"></i>
                           </a>
                            
                           @if(isAllowed($question->user_id))
                           <a class="btn btn-icon command-delete"  href="{{ route('question.index') }}" data-row-id="{{ $question->id }}" data-toggle="tooltip" data-placement="top" data-original-title="@lang('common.delete') {{ $question->title }}"><i class="md mk md-delete"></i></a>
                           @endif
                    </td>
                </tr>
                @endforeach            
            </tbody>
        </table>
    </div>
@endsection