<div class="card-body card-padding">

  @include('partials.form-errors')

  {!! csrf_field() !!}
    
    <!-- title, expired_date Form Input -->
    <div class="form-group">
      
      {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label required']) !!}
      <div class="col-sm-4">
          <div class="fg-line">
              {!! Form::textarea('title', null, ['class' => 'form-control input-sm', 'rows' => 5]) !!}
          </div>
      </div>

      {!! Form::label('expired_date', 'Expired Date:', ['class' => 'col-sm-2 control-label required']) !!}
        <div class="col-sm-4">
            <div class="fg-line">
                {!! Form::text('expired_date', null, ['class' => 'form-control input-sm date-time-picker']) !!}
            </div>
        </div>

    </div>

    <!-- Answers Form Input -->
    <div class="form-group">

      <div class="form-group form-group-options">
          {!! Form::label('answers[]', 'Answers:', ['class' => 'col-sm-2 control-label required']) !!}
          <div class="col-sm-10">
         
          @for($i = 0; $i < count($answers) + 1 ; $i++)
              <div class="input-group input-group-option">
                  <div class="fg-line">
                      {!! Form::text('answers[' . $i . ']', (isset($answers[$i]['title'])) ? $answers[$i]['title'] : null, ['class' => 'form-control input-sm']) !!}
                  </div>
                      <span class="input-group-addon last input-group-addon-remove">
                          <i class="md md-remove-circle-outline"></i>
                      </span>
              </div>
          @endfor
          </div>
      </div>

    </div>


    <!-- Created On Form Input -->
    @if(!empty($question->created_at) )
    <div class="form-group ">
         {!! Form::label('created_at', @trans('common.created_on'). ':', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
            <div class="fg-line">
            {!! Form::label('', $question->created_at, ['class' => 'control-label']) !!}
            </div>
        </div>
    </div>
    @endif

    <!-- Last Updated Form Input -->
    @if(!empty($question->updated_at) )
    <div class="form-group top">
         {!! Form::label('updated_at', @trans('common.last_update').':', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
            <div class="fg-line">
            {!! Form::label('', $question->updated_at, ['class' => 'control-label']) !!}
            </div>
        </div>
    </div>
    @endif

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-sm">@lang('common.save')</button>
            <a class="btn btn-info btn-sm" href="{{ route('question.index') }}">@lang('common.back')</a>
        </div>
    </div>

</div>
   

  
    