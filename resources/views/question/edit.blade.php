@extends('app')

@section('content')
    <div class="card">

        {!! Form::model($question, ['method' => 'put', 'url' => route('question.index') .'/'. $question->id, 'class' => 'form-horizontal']) !!}
            <div class="card-header">
                <h2>@lang('common.edit') {{ $question->name }}</h2>
            </div>

            @include('question.form')
        {!! Form::close() !!}
    </div>
@endsection
