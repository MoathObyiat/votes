@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Portal Dashboard</div>

                <div class="panel-body">
                    You are logged in as <strong>Portal</strong>
                        
                            @foreach ($questions as $question)
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('portal.votes') }}">
                                    {{ csrf_field() }}
                                    
                                    <h3>{{ $question->title }} - Expired Date: {{ $question->expired_date }}</h3>
                                    
                                    @if($question->expired_date < date("Y-m-d H:i:s") && !isVoted($question->id))
                                        <p><strong>Results:</strong> {{ number_format( ($question->vote->count() / $total) * 100, 2) }} %</p>
                                    @endif
                                    
                                    @foreach ($question->vote as $vote)
                                        @if($vote->is_votes)
                                            <p><strong>Results:</strong> {{ number_format( ($question->vote->count() / $total) * 100, 2) }} %</p>

                                            <p><strong>Your Answer:</strong> {{ $vote->answer->title }} </p>
                                        @endif
                                    @endforeach

                                    @foreach ($question->answers as $answer)
                                    <div class="radio">
                                      <label><input type="radio" name="answer_id" value="{{ $answer->id }}">{{ $answer->title }}</label>
                                    </div>
                                    @endforeach


                                    @if($question->expired_date > date("Y-m-d H:i:s") && !isVoted($question->id))
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Votes
                                            </button>
                                        </div>
                                    </div>
                                    @endif

                                    <input type="hidden" name="question_id" value="{{ $question->id }}">

                                </form>
                                <hr>
                            @endforeach

                            {{ $questions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection