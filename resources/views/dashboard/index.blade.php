@extends('app')

@section('content')

    <div class="block-header">
        <h2><i class="zmdi zmdi-view-dashboard zmdi-hc-fw"></i> @lang('dashboard.page_title')</h2>
        <div class="container">
	        <div class="row">&nbsp</div>

	        <div style="width:100%;">
			    {!! $chartjs->render() !!}
			</div>
                 
        </div>
    </div>

@endsection