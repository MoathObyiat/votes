<?php

// Question CRUD
Route::get('question', [
    'uses' => 'QuestionController@index',
    'as' => 'question.index'
]);

Route::get('question/{id}/show', [
    'uses' => 'QuestionController@show',
    'as' => 'question.show'
]);

Route::get('question/create', [
    'uses' => 'QuestionController@create',
    'as' => 'question.create'
]);

Route::post('question', [
    'uses' => 'QuestionController@store',
    'as' => 'question.store'
]);

Route::get('question/{id}/edit', [
    'uses' => 'QuestionController@edit',
    'as' => 'question.edit'
]);

Route::put('question/{id}', [
    'uses' => 'QuestionController@update',
    'as' => 'question.update'
]);

Route::post('question/{id}/delete', [
    'uses' => 'QuestionController@destroy',
    'as' => 'question.destroy'
]);
