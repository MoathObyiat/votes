<?php

// Dashboard
Route::get('locale/{locale}', [
    'uses' => 'DashboardController@changeLocale',
    'as' => 'dashboard.changeLocale',
    'permission' => 'dashboard'
]);