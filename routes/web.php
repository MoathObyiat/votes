<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return redirect()->route('login');
});

Route::get('/', function () {
    return redirect()->route('portal.login');
});

Auth::routes();

App::setLocale( Request::segment(1) != null ? Request::segment(1) : 'en' );

Route::group(['prefix' => App::getLocale()], function () {

    // Protected Routes by auth and acl middleware
    Route::group(['middleware' => ['web', 'auth']], function () {
        
        Route::get('dashboard', [
            'uses' => 'DashboardController@index',
            'as' => 'dashboard',
            'permission' => 'dashboard',
            'menuItem' => ['icon' => 'md md-dashboard', 'title' => 'Dashboard']
        ]);
        
        // Dynamically include all files in the routes directory
        foreach (new DirectoryIterator(__DIR__ . '/') as $file)
         { 
            if (!$file->isDot() && !$file->isDir() && $file->getFilename() != '.gitignore')
            {
                require_once __DIR__ . '/' . $file->getFilename();
            }
        }

    });
});



Route::prefix('portal')->group(function () {

    Route::get('/login', 'Auth\PortalLoginController@showLoginForm')->name('portal.login');

    Route::post('/login', 'Auth\PortalLoginController@login')->name('portal.login.submit');

    Route::get('/register', 'Auth\PortalRegisterController@showRegisterForm')->name('portal.register');

    Route::post('/register', 'Auth\PortalRegisterController@create')->name('portal.register.submit');

    Route::post('/logout', 'Auth\PortalRegisterController@logout')->name('portal.logout');

    Route::get('/', 'PortalController@index')->name('portal.index');

    Route::post('/votes', 'PortalController@saveVotes')->name('portal.votes');
});
