<?php

use App\Models\Vote;
use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Question::class, 100)->create()->each(function($question) {
                factory(App\Models\Answer::class, 4)->create(['question_id'=>$question->id])
                ->each(
                    function($answer) use (&$question) { 
                        
                        validateFaker($question->id, $answer->id);
                    }
                );
            }
        );
    }
}
