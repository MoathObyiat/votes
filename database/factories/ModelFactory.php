<?php

use App\Models\User;
use App\Models\Vote;
use App\Models\Voters;
use App\Models\Answer;
use App\Models\Question;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'      => $faker->name,
        'email'     => $faker->unique()->safeEmail,
        'password'  => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Voters::class, function (Faker\Generator $faker) {
	static $password;

    return [
        'name'      => $faker->name,
        'email'     => $faker->unique()->safeEmail,
        'password'  => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Question::class, function (Faker\Generator $faker) {

    return [
        'title'        => $faker->name,
        'user_id'      => User::all()->random()->id,
        'expired_date' => $faker->dateTimeBetween('-1 month', '+1 month'),
    ];
});


$factory->define(Answer::class, function (Faker\Generator $faker) {

    return [
        'title'        => $faker->name,
        'question_id'  => Question::all()->random()->id,
    ];
});

// $factory->define(Vote::class, function (Faker\Generator $faker) {

// 	$voter_id    = Voters::all()->random()->id;
// 	$question_id = Question::all()->random()->id;
// 	$answer_id   = Answer::all()->random()->id;

// 	$vote = Vote::where('question_id', $question_id)->where('voter_id', $voter_id)->exists();
	
// 	if(!$vote)
// 	    return [
// 	        'voter_id'     => $voter_id,
// 	        'question_id'  => $question_id,
// 	        'answer_id'    => $answer_id,
// 	    ];
// });